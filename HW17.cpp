﻿#include <iostream>

using namespace std;

class Stack
{
private:
	int size, last_index=0;
	int *dinmas;
public:
	Stack() : size(1)
	{
		dinmas = new int[size];
	}
	~Stack()
	{
		delete[] dinmas;
	}
	void DINMASS()
	{	for (int i = 0; i < last_index; i++) 
		{	cout << "dinmas[" << i << "]=" << dinmas[i] << "\n";
		}
	}
	int PUSH()
	{	int new_elem;
		cin >> new_elem;
		if (last_index >= size)
		{
			int* new_dinmas;
			size = size * 2;
			new_dinmas = new int[size];
			for (int i = 0; i < size; i++)
			{
				new_dinmas[i] = dinmas[i];
			}
			dinmas = new_dinmas;

		}
		dinmas[last_index] = new_elem;
		last_index++;
		return 0;
	}
	int POP()
	{	last_index--;
		return dinmas[last_index];
	}
};


int main()
{
	Stack v;
	v.PUSH();
	v.PUSH();
	v.PUSH();
	v.PUSH();
	v.PUSH();
	v.DINMASS();
}